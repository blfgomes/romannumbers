#!/usr/bin/env python
import unittest
from roman import convert

class TestConvert(unittest.TestCase):
    
    def setUp(self):
        pass
        
    
    def test_numbers_1_to_3(self):
        self.assertEqual(convert(1), 'I')
        self.assertEqual(convert(2), 'II')
        self.assertEqual(convert(3), 'III')
        
    def test_number_4(self):
        self.assertEqual(convert(4), 'IV')
        
    def test_number_5(self):
        self.assertEqual(convert(5), 'V')
        
    def test_numbers_6_to_10(self):
        self.assertEqual(convert(6), 'VI')
        self.assertEqual(convert(7), 'VII')
        self.assertEqual(convert(8), 'VIII')
        self.assertEqual(convert(9), 'IX')
        self.assertEqual(convert(10), 'X')
        
    def test_numbers(self):
        self.assertEqual(convert(49), 'XLIX')
        self.assertEqual(convert(267), 'CCLXVII')
        self.assertEqual(convert(499), 'CDXCIX')
        self.assertEqual(convert(1947), 'MCMXLVII')
        
if __name__ == '__main__':
    unittest.main()

