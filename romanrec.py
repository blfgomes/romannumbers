#!/usr/bin/env python

BLOCKS = [['M', 1000], ['CM', 900], ['D', 500], ['CD', 400], ['C', 100], ['XC', 90],
          ['L', 50], ['XL', 40], ['X', 10], ['IX', 9], ['V', 5], ['IV', 4], ['I', 1]]

def __convert_step__(decimal, remaining_blocks):
    if decimal == 0:
        return ''
    else:
        if decimal < remaining_blocks[0][1]:
            return __convert_step__(decimal, remaining_blocks[1:])
        else:
            return (remaining_blocks[0][0] + __convert_step__((decimal - remaining_blocks[0][1]), remaining_blocks))

def convert(decimal):
    return __convert_step__(decimal, BLOCKS)
